package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day3 {
    public static void main(String[] args) throws IOException, URISyntaxException {
        List<String> rates = getInputList("3");

        int[] ones = getCounts(rates, true);
        int[] zeros = getCounts(rates, false);

        long gamma = 0;
        long epsilon = 0;

        for(int i = 0; i < ones.length; i++) {

            double bitsToDecimal = Math.pow(2, ones.length - (i + 1));

            if(ones[i] > zeros[i]) {
                gamma += bitsToDecimal;
            } else {
                epsilon += bitsToDecimal;
            }
        }

        System.out.printf("Power consumption: %s\n", gamma * epsilon);

        List<String> oxygenRate = new ArrayList<>();
        List<String> co2Rate = new ArrayList<>();
        for (int i = 0; i < ones.length; i++) {
            if(i == 0) {
                oxygenRate = getIterationListOxygen(rates, i);
                co2Rate = getIterationListCO2(rates, i);
            } else {
                oxygenRate = getIterationListOxygen(oxygenRate, i);
                co2Rate = getIterationListCO2(co2Rate, i);
            }
        }

        System.out.printf("Life support rating is: %s", Integer.parseInt(oxygenRate.get(0), 2) * Integer.parseInt(co2Rate.get(0), 2));
    }

    private static int[] getCounts(final List<String> rates, final boolean arrayTypeOne) {

        int[] ones = new int[rates.get(0).length()];
        int[] zeros = new int[rates.get(0).length()];

        for (String rate : rates) {
            for (int i = 0; i < rate.length(); i++) {
                if(rate.charAt(i) == '0') {
                    zeros[i]++;
                } else {
                    ones[i]++;
                }
            }
        }

        System.out.printf("%s %s %s %s %s\n", ones[0], ones[1], ones[2], ones[3], ones[4]);
        System.out.printf("%s %s %s %s %s\n", zeros[0], zeros[1], zeros[2], zeros[3], zeros[4]);
        return arrayTypeOne ? ones : zeros;
    }

    private static List<String> getIterationListOxygen(final List<String> rates, final int index) {
        System.out.printf("index: %s, rates: %s\n", index, rates);

        List<String> oxygenRates = new ArrayList<>();

        int[] ones = getCounts(rates, true);
        int[] zeros = getCounts(rates, false);

        for (String rate : rates) {
            if(ones[index] >= zeros[index]) {
                if(rate.charAt(index) == '1') {
                    oxygenRates.add(rate);
                }
            } else {
                if(rate.charAt(index) == '0') {
                    oxygenRates.add(rate);
                }
            }
        }
        System.out.println(oxygenRates);
        return oxygenRates;
    }

    private static List<String> getIterationListCO2(final List<String> rates, final int index) {
        System.out.printf("index: %s, rates: %s\n", index, rates);

        if(rates.size() == 1) {
            return rates;
        } else {
            List<String> co2Rates = new ArrayList<>();

            int[] ones = getCounts(rates, true);
            int[] zeros = getCounts(rates, false);

            for (String rate : rates) {
                if (ones[index] >= zeros[index]) {
                    if (rate.charAt(index) == '0') {
                        co2Rates.add(rate);
                    }
                } else {
                    if (rate.charAt(index) == '1') {
                        co2Rates.add(rate);
                    }
                }
            }
            System.out.println(co2Rates);
            return co2Rates;
        }
    }
}