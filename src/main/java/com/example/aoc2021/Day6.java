package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.*;

import static com.example.aoc2021.AOCReader.getInputList;
import static java.util.Arrays.asList;
import static java.util.stream.Collectors.toList;

public class Day6 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        List<Long> inputList = getInputList("6")
                .stream()
                .map(e -> asList(e.split(",")))
                .flatMap(Collection::stream)
                .map(Long::parseLong)
                .collect(toList());

        int daysP1 = 80;
        int daysP2 = 256;

        long start1 = System.nanoTime();
        long part1 = simulateDays(daysP1, inputList);
        long end1 = System.nanoTime();

        System.out.printf("After %s gives %s lantern fish and took %s ms \n", daysP1, part1, (end1 - start1) / 1000);

        long start2 = System.nanoTime();
        long part1Opt = simulate(inputList, daysP1);
        long end2 = System.nanoTime();

        long start3 = System.nanoTime();
        long part2 = simulate(inputList, daysP2);
        long end3 = System.nanoTime();

        System.out.printf("Optimized: After %s days gives %s lantern fish and took %s ms \n", daysP1, part1Opt, (end2 - start2) / 1000);
        System.out.printf("Optimized: After %s days gives %s lantern fish and took %s ms \n", daysP2, part2, (end3 - start3) /1000);
    }

    private static long simulate(final List<Long> inputList, final int day) {
        Map<Long, Long> fishes = new HashMap<>();
        inputList.forEach(life -> fishes.put(life, fishes.getOrDefault(life, 0L) + 1));

        for (int i = 0; i < day; i++) {
            for (long life = 0; life <= 8; life++) {
                fishes.put(life - 1, fishes.getOrDefault(life, 0L));
            }

            fishes.put(6L, fishes.getOrDefault(6L, 0L) + fishes.get(-1L));
            fishes.put(8L, fishes.remove(-1L));
        }

        long sum = 0;
        for (long count : fishes.values()) {
            sum += count;
        }

        return sum;
    }

    private static long simulateDays(final int day, final List<Long> inputList) {
        List<Long> updatedList = new ArrayList<>();

        int dayCount = 1;
        while(dayCount <= day) {
            updatedList = dayCount == 1 ?
                    inputList.stream().map(input -> input - 1).collect(toList()) :
                    handleNextDay(updatedList);

            //System.out.println(dayCount);
            dayCount++;
        }

        //System.out.printf("Simulated day %s gives: %s\n", day, updatedList);

        return updatedList.size();
    }

    private static List<Long> handleNextDay(final List<Long> inputList) {
        long numberOfZeros = inputList.stream().filter(e -> e == 0).count();
        List<Long> updatedList = inputList.stream().map(input -> input == 0 ? 6 : input - 1).collect(toList());

        while (numberOfZeros > 0L) {
            updatedList.add(8L);
            numberOfZeros--;
        }

        //System.out.println(updatedList);
        return updatedList;
    }
}
