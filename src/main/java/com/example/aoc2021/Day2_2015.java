package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day2_2015 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> measurements = getInputList("22015");

        long sumP1 = 0;
        long sumP2 = 0;
        for (String measurement : measurements) {
            String[] sizes = measurement.split("x");

            //l = pos 0, w = pos 1, h = pos 2
            int lw = Integer.parseInt(sizes[0]) * Integer.parseInt(sizes[1]);
            int wh = Integer.parseInt(sizes[1]) * Integer.parseInt(sizes[2]);
            int lh = Integer.parseInt(sizes[2]) * Integer.parseInt(sizes[0]);
            int area = 2 * lw + 2 * wh + 2 * lh + Math.min(Math.min(lw, lh), wh);

            sumP1 += area;

            int l = Integer.parseInt(sizes[0]);
            int w = Integer.parseInt(sizes[1]);
            int h = Integer.parseInt(sizes[2]);

            sumP2 += Math.min(Math.min(2*(l+w), 2*(l+h)), 2*(w+h)) + l*w*h;
        }
        System.out.printf("Sum of present paper sqf: %s \nSum of minimal ribbon: %s", sumP1, sumP2);
    }
}
