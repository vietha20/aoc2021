package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static java.lang.String.format;

public class AOCReader {
    public static List<String> getInputList(String day) throws URISyntaxException, IOException {
        String input = format("d%s", day);
        Path path = Paths.get(AOCReader.class.getClassLoader().getResource(input).toURI());
        return Files.lines(path).filter(Objects::nonNull).collect(Collectors.toList());
    }
}
