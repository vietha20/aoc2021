package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day1_2015 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> lines = getInputList("12015");
        List<Integer> positions = new ArrayList<>();

        int floor = 0;
        String firstLine = lines.get(0);
        for (int i = 0; i < firstLine.length(); i++) {

            if(firstLine.charAt(i) == '(') {
                floor += 1;
            } else {
                floor -= 1;
            }

            if(floor == -1) {
                positions.add(i+1);

            }
        }

        System.out.printf("Ended on floor: %s\n", floor);
        System.out.printf("Floor ended on -1 at min position: %s\n", positions.stream().min(Comparator.naturalOrder()).get());
    }
}
