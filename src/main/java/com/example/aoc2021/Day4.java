package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import static com.example.aoc2021.AOCReader.getInputList;
import static java.util.Arrays.stream;
import static java.util.Collections.singleton;
import static java.util.stream.Collectors.toList;

public class Day4 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        long start = System.nanoTime();
        List<String> inputList = getInputList("4");
        List<Integer> draws = stream(inputList.get(0).split(",")).map(Integer::parseInt).collect(toList());

        List<int[][]> boards = getBingoBoards(inputList);

        for (Integer draw : draws) {
            drawBoard(boards, draw);

            boolean done = false;
            for (int[][] board : boards) {
                if (won(board)) {
                    long leftOver = findLeftOver(board);
                    done = true;
                    long end = System.nanoTime();
                    System.out.printf("Leftover: %s, Draw: %s, Final score: %s, Time: %s ms \n", leftOver, draw, leftOver * draw, (end - start)/Math.pow(10, 6));
                    break;
                }
            }

            if (done)
                break;
        }
    }

    private static long findLeftOver(int[][] board) {
        long leftOver = 0;
        for(int[] row : board) {
            for(int col : row) {
                if(col != -1) {
                    leftOver += col;
                }
            }
        }

        return leftOver;
    }

    private static boolean won(int[][] board) {
        boolean won;

        for (int row = 0; row < 5; row++) {
            won = true;
            //System.out.printf("board: %s\n", Arrays.toString(board[row]));
            for (int col = 0; col < 5; col++) {
                if(board[row][col] != -1) {
                    won = false;
                    break;
                }
            }

            if(won) {
                return true;
            }
        }


        for (int col = 0; col < 5; col++) {
            won = true;
            for (int row = 0; row < 5; row++) {
                //System.out.printf("board: %s\n", Arrays.toString(board[row]));
                if(board[row][col] != -1) {
                    won = false;
                    break;
                }
            }

            if(won) {
                return true;
            }
        }

        return false;
    }

    private static void drawBoard(List<int[][]> boards, int draw) {
        for (int[][] board : boards) {
            for (int row = 0; row < board.length; row++) {
                for (int col = 0; col < board[row].length; col++) {
                    //System.out.printf("board[row][col]: %s, draw: %s row: %s\n", board[row][col], draw, Arrays.toString(board[row]));
                    if(board[row][col] == draw) {
                        board[row][col] = -1;
                    }
                }
            }
        }
    }

    private static List<int[][]> getBingoBoards(final List<String> inputList) {

        inputList.remove(0);
        inputList.removeAll(singleton(""));

        List<List<String>> boardLines = inputList.stream()
                .map(e -> stream(e.split(" "))
                        .filter(e1 -> !e1.isEmpty())
                        .collect(toList()))
                .collect(toList());

        List<int[][]> boards = new ArrayList<>();
        for (int i = 0; i < inputList.size(); i += 5) {
            int[][] board = new int[5][5];
            for (int j = 0; j < 5; j++) {
                var line = boardLines.get(i + j);
                int k = 0;
                for (String number : line) {
                    board[j][k++] = Integer.parseInt(number);
                }
            }
            boards.add(board);
        }
        return boards;
    }
}
