package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day1 {
    public static void main(String[] args) throws IOException, URISyntaxException {
        List<Integer> depths = getInputList("1").stream().map(Integer::parseInt).collect(Collectors.toList());

        int changes1 = getChanges(depths);
        System.out.println(changes1);

        List<Integer> windows = new ArrayList<>();
        for(int i = 2; i< depths.size(); i++) {
            windows.add(depths.get(i) + depths.get(i - 1) + depths.get(i - 2));
        }
        int changes2 = getChanges(windows);
        System.out.println(changes2);
    }

    public static int getChanges(final List<Integer> depthsOrWindow) {
        int changes = 0;
        for(int i = 1; i< depthsOrWindow.size(); i++) {
            if(depthsOrWindow.get(i) > depthsOrWindow.get(i - 1)) {
                changes++;
            }
        }

        return changes;
    }
}
