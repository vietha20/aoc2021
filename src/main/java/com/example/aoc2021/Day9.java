package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day9 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> smokeFlows = getInputList("9");

        int[][] flowMatrix = new int[smokeFlows.size()][smokeFlows.get(0).length()];
        for (int i = 0; i < smokeFlows.size(); i++) {
            String[] currentLine = smokeFlows.get(i).split("");

            for (int j = 0; j < currentLine.length; j++) {
                flowMatrix[i][j] = Integer.parseInt(currentLine[j]);
            }
        }

        int sum = findSumOfLowestPoints(flowMatrix);
        System.out.printf("Sum of lowest points: %s", sum);
    }

    private static int findSumOfLowestPoints(int[][] flowMatrix) {

        int sum = 0;
        for (int row = 0; row < flowMatrix.length; row++) {
            for (int col = 0; col < flowMatrix[row].length; col++) {
                int currentPoint = flowMatrix[row][col];

                if ((row - 1 >= 0 && currentPoint >= flowMatrix[row - 1][col]) ||
                        (row + 1 < flowMatrix.length && currentPoint >= flowMatrix[row + 1][col]) ||
                        (col - 1 >= 0 && currentPoint >= flowMatrix[row][col - 1]) ||
                        (col + 1 < flowMatrix[row].length && currentPoint >= flowMatrix[row][col + 1])) {
                    continue;
                }

                sum += currentPoint + 1;

            }
        }

        return sum;
    }
}
