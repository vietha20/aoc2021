package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day5 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> lines = getInputList("5");

        Map<Point, Point> ventMap = new HashMap<>();
        for (String line : lines) {
            System.out.println(line);
            String[] coords = line.split(" -> ");
            String[] p1 = coords[0].split(",");
            String[] p2 = coords[1].split(",");

            ventMap.put(new Point(p2), new Point(p1));
            //System.out.println(ventMap);
        }

        Map<Point, Integer> points = new HashMap<>();
        for (Map.Entry<Point, Point> a : ventMap.entrySet()) {
            Point p1 = a.getKey();
            Point p2 = a.getValue();

            System.out.printf("p1: %s p2: %s\n", p1, p2);
            if (p1.y == p2.y) {
                for (int i = Math.min(p1.x, p2.x); i <= Math.max(p1.x, p2.x); i++) {
                    points.merge(new Point(i, p1.y), 1, Integer::sum);
                }
            } else if (p1.x == p2.x) {
                for (int i = Math.min(p1.y, p2.y); i <= Math.max(p1.y, p2.y); i++) {
                    points.merge(new Point(p1.x, i), 1, Integer::sum);
                }
            }
        }

        int count = 0;
        for (int visits : points.values()) {
            if (visits > 1) {
                count += 1;
            }
        }

        System.out.printf("Total number of overlaps: %s", count);
    }

    public static class Point {
        private int x;
        private int y;

        public Point(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public Point(String[] points) {
            this.x = Integer.parseInt(points[0]);
            this.y = Integer.parseInt(points[1]);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Point point = (Point) o;
            return x == point.x && y == point.y;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }

        @Override
        public String toString() {
            return "{x = " + this.x + ", y = " + this.y + "}";
        }
    }
}
