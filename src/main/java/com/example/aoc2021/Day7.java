package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static com.example.aoc2021.AOCReader.getInputList;
import static java.util.Arrays.stream;

public class Day7 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        List<String> inputs = stream(getInputList("7").get(0).split(",")).collect(Collectors.toList());

        int[] iterationCountsPart1 = new int[inputs.size()];
        int[] iterationCountsPart2 = new int[inputs.size()];
        for (int i = 0; i < inputs.size(); i++) {
            for (String input : inputs) {
                int sum = IntStream.range(0, Math.abs(Math.abs(Integer.parseInt(input)) - i)).sum();
                iterationCountsPart1[i] += Math.abs(Integer.parseInt(input) - i);
                iterationCountsPart2[i] += Math.abs(Integer.parseInt(input) - i) + sum;
            }
        }

        System.out.printf("Minimum fuel cost: %s \n", stream(iterationCountsPart1).min().getAsInt());
        System.out.printf("Minimum fuel cost: %s \n", stream(iterationCountsPart2).min().getAsInt());
    }
}
