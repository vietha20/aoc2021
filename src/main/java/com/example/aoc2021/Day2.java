package com.example.aoc2021;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import static com.example.aoc2021.AOCReader.getInputList;

public class Day2 {
    public static void main(String[] args) throws URISyntaxException, IOException {
        getPlannedCourse(getInputList("2"));
        getPlannedCoursePart2(getInputList("2"));
    }

    public static void getPlannedCourse(final List<String> commands) {
        int depths = 0;
        int position = 0;

        for (String command : commands) {
            String[] commandArr = command.split(" ");
            if(commandArr[0].equals("forward")) {
                position = position + Integer.parseInt(commandArr[1]);
            } else if(commandArr[0].equals("up")) {
                depths = depths - Integer.parseInt(commandArr[1]);
            } else if(commandArr[0].equals("down")) {
                depths = depths + Integer.parseInt(commandArr[1]);
            } else {
                // not here
            }
        }

        System.out.printf("Final planned course: %s \n", depths * position);
    }

    public static void getPlannedCoursePart2(final List<String> commands) {
        int aim = 0;
        int position = 0;
        int depths = 0;

        for (String command : commands) {
            String[] commandArr = command.split(" ");
            if(commandArr[0].equals("forward")) {
                position = position + Integer.parseInt(commandArr[1]);
                depths = Integer.parseInt(commandArr[1]) * aim + depths;
            } else if(commandArr[0].equals("up")) {
                aim = aim - Integer.parseInt(commandArr[1]);
            } else if(commandArr[0].equals("down")) {
                aim = aim + Integer.parseInt(commandArr[1]);
            } else {
                // not here
            }
        }

        System.out.printf("Final depth: %s \n", depths * position);
    }
}